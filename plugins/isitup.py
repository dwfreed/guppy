#    guppy Copyright (C) 2010-2011 guppy team members.
#
#    This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
#    This is free software, and you are welcome to redistribute it
#    under certain conditions; type `show c' for details.
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.


import urllib.request, urllib.parse, urllib.error, json

@plugin
class IsItUp(object):
    """Check domain status using isitup.org"""
    def __init__(self, server):
        self.server = server
        self.prnt = server.prnt
        self.commands = ["isitup"]
        self.server.handle("command", self.handle_command, self.commands)
    
    def handle_command(self, channel, user, cmd, args):
        if len(args) < 1:
            self.server.doMessage(channel, user+": Not enough arguments.")
            return

        if args[0].startswith("http://"):
            url = args[0][7:]
        elif args[0].startswith("https://"):
            url = args[0][8:]
        else:
            url = args[0]        
        code = "".join(x.decode('utf8') for x in urllib.request.urlopen("http://isitup.org/"+url+".json").readlines())
        #code = code.decode('utf8')
        print(code)
        arr = json.loads(code)
        arr['up'] = 'up' if arr['status_code'] == 1 else 'not up'
        if arr['up'] == 'up':self.server.doMessage(channel, user+': %(domain)s:%(port)s is %(up)s. Got HTTP status code %(response_code)03d from %(response_ip)s in %(response_time)f ms.' % arr)
        else: self.server.doMessage(channel, user+': %s is not up.' % arr['domain'])

