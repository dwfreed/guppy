#    guppy Copyright (C) 2010-2011 guppy team members.
#
#    This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
#    This is free software, and you are welcome to redistribute it
#    under certain conditions; type `show c' for details.
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

# dns module
# --gry

import urllib.request, urllib.parse, urllib.error
import socket

@plugin
class dns(object):
    """DNS lookups."""
    def __init__(self, server):
        self.server = server
        self.commands = ["dns"]
        self.server.handle("command", self.handle_command, self.commands)
    
    def handle_command(self, channel, user, cmd, args):
        if cmd == "dns":
            if len(args) < 1:
                self.server.doMessage(channel, user+": Return a fully qualified domain name for a list of space-separated IPs or hostnames.")
                return
            try:
                for each in args:
                    self.server.doMessage(channel, user+": "+ each+" = "+socket.getfqdn(each))
            except Exception as e:
                self.server.doMessage(channel, user+": "+str(e))

