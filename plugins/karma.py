#    guppy Copyright (C) 2010-2011 guppy team members.
#
#    This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
#    This is free software, and you are welcome to redistribute it
#    under certain conditions; type `show c' for details.
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

import re, configparser, os

@plugin
class Karma(object):
    def __init__(self, server):
        self.server = server
        self.prnt = server.prnt
        
        self.commands = [ "karma", "points" ]
        
        self.server.handle("message", self.handle_message)
        self.server.handle("command", self.handle_command, self.commands)
        
        self.userpoints = { }
        self.karmadbfile = self.server.config["confdir"] + "karma.cfg"
        
        self.karma_re = re.compile("^.+(\+\+|\-\-).*$")
        
        self.configParser = configparser.RawConfigParser()
        if os.path.isfile(self.karmadbfile):
            self.configParser.read(self.karmadbfile)
            if self.configParser.has_section(self.server.config["network"]):
                for user,pts in self.configParser.items(self.server.config["network"]):
                    self.userpoints[user] = int(pts)
    
    def destroy(self):
        self.configParser = configparser.RawConfigParser()
        if os.path.isfile(self.karmadbfile):
            self.configParser.read(self.karmadbfile)
        
        network = self.server.config["network"]
        if not self.configParser.has_section(network):
            self.configParser.add_section(network)
            
        for user,pts in list(self.userpoints.items()):
            self.configParser.set(network,user,str(pts))
        
        fh = open(self.karmadbfile, "w")
        self.configParser.write(fh)
        fh.close()
    
    def handle_command(self, channel, user, cmd, args):
        if len(args) < 1:
            self.server.doMessage(channel, user+": Displays the amount of karma a user has. Usage: karma <nickname>")
            return
        
        if cmd == "karma" or cmd == "points":
            nick = args[0].lower()
            if self.userpoints.get(nick, None) is None:
                self.userpoints[nick] = 0
                
            self.server.doMessage(channel, user+": "+args[0]+" has "+str(self.userpoints[nick])+" points.")
            
    def handle_message(self, channel, user, message):
        if self.karma_re.match(message):
            ulist = [u for u in message.split(";") if u != '']
            addlist = [u[:-2].strip(" ") for u in ulist if u.endswith("++")]
            sublist = [u[:-2].strip(" ") for u in ulist if u.endswith("--")]
            
            for nick in addlist:
                lnick = nick.lower()
                if self.userpoints.get(lnick, None) is None:
                    self.userpoints[lnick] = 1
                else:
                    total = self.userpoints[lnick] + 1
                    self.userpoints[lnick] = total
            
            for nick in sublist:
                lnick = nick.lower()
                if self.userpoints.get(lnick, None) is None:
                    self.userpoints[lnick] = -1
                else:
                    total = self.userpoints[lnick] - 1
                    self.userpoints[lnick] = total
